﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using System.Net;

namespace EhabAhmed_Evolvice
{
    public class DisposeClass
    {

        #region Dispose
        private bool disposed = false;
        private SafeFileHandle safeHandle = new SafeFileHandle(IntPtr.Zero, true);
        public static string domainURL = System.Configuration.ConfigurationManager.AppSettings["domainURL"];        

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            // Dispose of managed resources here.
            if (disposing)
                safeHandle.Dispose();

            // Dispose of any unmanaged resources not wrapped in safe handles.

            disposed = true;
        }
        #endregion
        protected virtual string ToJson(object list)
        {
            JavaScriptSerializer x = new JavaScriptSerializer();
            x.MaxJsonLength = 2147483644;
            var json = x.Serialize(list);
            return json;
        }
    }
}
