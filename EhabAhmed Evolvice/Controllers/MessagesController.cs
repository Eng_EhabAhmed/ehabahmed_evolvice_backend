﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EhabAhmed_Evolvice
{
    /// <summary>
    /// Messages Controller
    /// </summary>
    public class MessagesController : ApiController
    {
        public static readonly string controllerName = "Messages";

        /// <summary>
        /// Gets messages based on page number.
        /// </summary>
        /// <param name="page">The page number.</param>
        /// <returns>List of messages</returns>

        [Route("api/Messages/GetMessages/{page}")]
        public string GetMessages(int page)
        {
            MessageRepo repo = new MessageRepo();
            LogFiles log = new LogFiles();
            try
            {
                log.WriteToFile(log.logTrace, controllerName, "GetMessages", "call success");
                string result = repo.getMessages(page);
                log.Dispose();
                repo.Dispose();
                return result;
            }
            catch (Exception e)
            {
                log.WriteToFile(log.logError, controllerName, "GetMessages", e.ToString());
                log.Dispose();
                repo.Dispose();
                return "";
            }
        }

        /// <summary>
        /// Add new message
        /// </summary>
        /// <param name="MessageText">Message Text</param>
        /// <param name="CreatedDate">Creating date of message</param>
        /// <param name="ModifiedDate">Last modifing date of message</param>
        /// <param name="CreatedByUserId">UserId who created the message</param>
        /// <returns>Boolen : true in success and false in fail</returns>
        [HttpPost]
        public bool AddMessage(MessageContract messageContract)
        {
            MessageRepo db = new MessageRepo();
            LogFiles log = new LogFiles();
            try
            {
                log.WriteToFile(log.logTrace, controllerName, "AddMessage", "call success");
                bool result = db.addMessage(messageContract);
                log.Dispose();
                db.Dispose();
                return result;
            }
            catch (Exception e)
            {
                log.WriteToFile(log.logError, controllerName, "AddMessage", e.ToString());
                log.Dispose();
                db.Dispose();
                return false;
            }

        }
    }
}
