﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EhabAhmed_Evolvice.Controllers
{
    /// <summary>
    /// Users Controller
    /// </summary>
    public class UsersController : ApiController
    {
        public static readonly string controllerName = "Users";

        /// <summary>
        /// Gets user based on user id.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>User</returns>
        [Route("api/Users/GetUserById/{id}")]
        public string GetUserById(int id)
        {
            UserRepo repo = new UserRepo();
            LogFiles log = new LogFiles();
            try
            {
                log.WriteToFile(log.logTrace, controllerName, "GetUserById", "call success");
                string result = repo.getUserById(id);
                log.Dispose();
                repo.Dispose();
                return result;
            }
            catch (Exception e)
            {
                log.WriteToFile(log.logError, controllerName, "GetUserById", e.ToString());
                log.Dispose();
                repo.Dispose();
                return "";
            }
        }

        /// <summary>
        /// Gets users.
        /// </summary>
        /// <returns>List of users</returns>
        [Route("api/Users/GetUsers")]
        public string GetUsers()
        {
            UserRepo repo = new UserRepo();
            LogFiles log = new LogFiles();
            try
            {
                log.WriteToFile(log.logTrace, controllerName, "GetUsers", "call success");
                string result = repo.getUsers();
                log.Dispose();
                repo.Dispose();
                return result;
            }
            catch (Exception e)
            {
                log.WriteToFile(log.logError, controllerName, "GetUsers", e.ToString());
                log.Dispose();
                repo.Dispose();
                return "";
            }
        }
    }
}
