﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EhabAhmed_Evolvice
{
    public class UserRepo : DisposeClass
    {
        UserDB userDB = new UserDB();
        internal string getUserById(int id)
        {
            UserContract user = userDB.getUserById(id);
            userDB.Dispose();
            return ToJson(user);
        }

        internal string getUsers()
        {
            List<UserContract> users = userDB.getUsers();
            userDB.Dispose();
            return ToJson(users);
        }
    }
}