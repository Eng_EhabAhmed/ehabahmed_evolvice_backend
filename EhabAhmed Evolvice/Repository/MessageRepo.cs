﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EhabAhmed_Evolvice
{
    public class MessageRepo : DisposeClass
    {
        MessageDB messageDB = new MessageDB();
        internal string getMessages(int page)
        {
            List<MessageContract> messages = messageDB.getMessages(page);
            messageDB.Dispose();
            return ToJson(messages);
        }

        internal bool addMessage(MessageContract message)
        {
            bool result = messageDB.addMessage(message);
            messageDB.Dispose();
            return result;
        }
    }
}