﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EhabAhmed_Evolvice
{
    /// <summary>
    /// Message Contract
    /// </summary>
    public class MessageContract
    {
        public int id { get; set; }
        public string messageText { get; set; }
        public int parentMessageId { get; set; }
        public string createdDate { get; set; }
        public string modifiedDate { get; set; }
        public int createdByUserId { get; set; }
    }
}