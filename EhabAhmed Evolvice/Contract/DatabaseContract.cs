﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EhabAhmed_Evolvice
{
    /// <summary>
    /// Database Contract
    /// To read data from databasefile and map it to users or messages contract
    /// </summary>

    public class DatabaseContract
    {
        public List<MessageContract> messages { get; set; }
        public List<UserContract> users { get; set; }

    }
}