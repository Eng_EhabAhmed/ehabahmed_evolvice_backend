﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EhabAhmed_Evolvice
{
    public class LogFiles
    {
        public  readonly string logTrace = "logTrace";
        public  readonly string logError = "logError";
        private bool disposed = false;
        private SafeFileHandle safeHandle = new SafeFileHandle(IntPtr.Zero, true);
        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            // Dispose of managed resources here.
            if (disposing)
                safeHandle.Dispose();

            // Dispose of any unmanaged resources not wrapped in safe handles.

            disposed = true;
        }  
        public  void WriteToFile(string fileName, string controller, string method, string message)
        {
            DateTime dateTime = DateTime.Now.ToUniversalTime();
            string logPath = AppDomain.CurrentDomain.BaseDirectory + "App_Data\\" + fileName + "\\" + dateTime.ToShortDateString().Replace('/','-') + ".txt";
            try
            {               
                using (StreamWriter sw = new StreamWriter(logPath, true))
                {                    
                    sw.WriteLine(dateTime.ToString());
                    sw.WriteLine(controller);
                    sw.WriteLine(method);
                    sw.WriteLine(message);
                    sw.WriteLine("");
                    sw.WriteLine("");
                    sw.Close();
                }
            }
            catch(Exception e){
            }
        }
    }
}