﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EhabAhmed_Evolvice
{
    public class Files : DisposeClass
    {
        /// <summary>
        /// Write into database file
        /// </summary>
        ///<param name="data">Data need to be written in the database file</param>
        public void WriteFile(string data)
        {
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "Database\\database.txt";
            try
            {
                //Remove the datasefile to be written again.
                File.WriteAllText(filePath, "");
                using (StreamWriter sw = new StreamWriter(filePath, true))
                {
                    sw.Write(data);
                    sw.Close();
                }
            }
            catch (Exception e)
            {
            }
        }

        /// <summary>
        /// Read from database file
        /// </summary>
        public string ReadFile()
        {
            string json = "";
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "Database\\database.txt";
            try
            {
                using (StreamReader sr = new StreamReader(filePath))
                {
                    string line;
                    //Read all lines from the database file.
                    while ((line = sr.ReadLine()) != null)
                    {
                        json += line;
                    }
                }

                return json;
            }
            catch (Exception e)
            {
                return "";
            }
        }
    }
}