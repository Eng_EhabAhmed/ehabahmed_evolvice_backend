﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace EhabAhmed_Evolvice
{
    public class UserDB : DisposeClass
    {
        /// <summary>
        /// Get a user based on id
        /// </summary>
        internal UserContract getUserById(int id)
        {
            UserContract user = getUsers().FirstOrDefault(x => x.id == id);
            return user;
        }

        /// <summary>
        /// Get all the users from database.
        /// </summary>
        internal List<UserContract> getUsers()
        {
            Files files = new Files();
            string database = files.ReadFile();
            files.Dispose();

            JavaScriptSerializer x = new JavaScriptSerializer();
            List<UserContract> usersList = x.Deserialize<DatabaseContract>(database).users;

            return usersList;

        }
    }
}