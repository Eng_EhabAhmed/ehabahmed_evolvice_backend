﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace EhabAhmed_Evolvice
{
    public class MessageDB : DisposeClass
    {
        /// <summary>
        /// Get all messages based on page number.
        /// </summary>
        internal List<MessageContract> getMessages(int page)
        {
            // pageLength is defined in config file and the default now is 5
            int pageLength = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["pageLength"]);

            //Read only messages from database
            List<MessageContract> messagesList = getDatabase().messages;

            //Order messages list by descending .
            //Skip based on number of page and pageLength.
            //Take the top messages based on pageLength.
            List<MessageContract> selectedMessages = messagesList.OrderByDescending(m=>m.modifiedDate)
            .Skip(pageLength * page)
            .Take(pageLength)
            .ToList();

            return selectedMessages;
        }

        /// <summary>
        /// Add new message.
        /// </summary>
        internal bool addMessage(MessageContract message)
        {
            try
            {
                Files files = new Files();

                DatabaseContract database = getDatabase();

                //Get the max message id to add new message with id greather than the max by 1
                int lastMessageId = database.messages.Select(m => m.id).Max();
                message.id = lastMessageId + 1;

                //Add the new message to existing database
                database.messages.Add(message);

                files.WriteFile(ToJson(database));
                files.Dispose();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        /// <summary>
        /// Get the database
        /// </summary>
        private DatabaseContract getDatabase()
        {
            //This method will help the above methods to read from database.
            Files files = new Files();
            string database = files.ReadFile();
            files.Dispose();

            JavaScriptSerializer x = new JavaScriptSerializer();
            DatabaseContract databaseList = x.Deserialize<DatabaseContract>(database);

            return databaseList;
        }
    }
}